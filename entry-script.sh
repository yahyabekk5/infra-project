#!/bin/bash

# Mise à jour du système et installation de Docker
sudo yum update -y
sudo yum install -y docker

# Démarrage du service Docker
sudo systemctl start docker

sudo usermod -aG docker ec2-user

# Téléchargement et exécution d'une image de serveur web (par exemple, nginx)
sudo docker run -d -p 8080:80 nginx

