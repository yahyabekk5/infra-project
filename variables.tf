variable region {}
variable access_key {}
variable secret_key {}
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}
variable instance_type {}
variable public_key_location {
    type    = string
} 
variable image_name {}
